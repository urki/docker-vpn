# VPN docker server: 

Ker nimamo več vpn serverja sem naredil novega. Ka je simpl!!!! 

* Video:

[![IMAGE ALT TEXT HERE](https://img.youtube.com/vi/M0Ywc6l7T_k/0.jpg)](https://www.youtube.com/watch?v=M0Ywc6l7T_k)

* Na routerju odpreš porte na ip kjer bo vpn. Npr:
![setup router](FirewallEntryType.png)

## Metoda 1  -  Random gesli ob vsakem zagonu
`docker run --name ipsec-vpn-server --restart=always -p 500:500/udp -p 4500:4500/udp -v /lib/modules:/lib/modules:ro -d --privileged hwdsl2/ipsec-vpn-server`  

* potem pa za password in navodila/gesla napises: 
`docker logs ipsec-vpn-server`

## Metoda 2 - Z fiksnimi passwordi in username

Narediš neko mapo in greš vanjo ter narediš vpn.env datoteko. Vanjo vpišeš željeno username in gesla. Npr:
```
VPN_IPSEC_PSK='DUEwbEGwUpLfiVKK9C4f'
VPN_USER='vpnuser'
VPN_PASSWORD='UsNhs74DTBp4rP5Z'
```
Če  zaženeš docker na mac/windowsih uporabiš še IP računalnika (npr: 10.0.1.4) in seveda ta port na istem računalniku ne sme še biti uporabljen
V isti mapi kjer je vpn.env zaženeš:

```
docker run \
    --name ipsec-vpn-server \
    --env-file ./vpn.env \
    --restart=always \
    -p 10.0.1.4:500:500/udp \
    -p 10.0.1.4:4500:4500/udp \
    -v /lib/modules:/lib/modules:ro \
    -d --privileged \
    hwdsl2/ipsec-vpn-server
```


Nastavitev VPN na macOS:
![add vpn](addVpn.png)
![setup vpn](setupVpn.png)



Povezava:

 * [https://youtu.be/M0Ywc6l7T_k](https://youtu.be/M0Ywc6l7T_k)
 
 * [https://hub.docker.com/r/hwdsl2/ipsec-vpn-server](https://hub.docker.com/r/hwdsl2/ipsec-vpn-server)






